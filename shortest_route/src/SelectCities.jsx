import React from "react";

const SelectCities = (props) => {
  const { startPoint, setStartPoint, endPoint, setEndPoint, cities } = props;
  return (
    <div style={{ marginLeft: 3 }}>
      <label htmlFor="from">Start</label>
      <select
        name="from"
        id="from"
        value={startPoint}
        onChange={(e) => setStartPoint(e.target.value)}
      >
        <option value="">-</option>
        {cities.map((city) => (
          <option key={city.name} value={city.name}>
            {city.name}
          </option>
        ))}
      </select>

      <label htmlFor="to">End</label>
      <select
        name="to"
        id="to"
        value={endPoint}
        onChange={(e) => setEndPoint(e.target.value)}
      >
        <option value="">-</option>
        {cities.map((city) => (
          <option key={city.name} value={city.name}>
            {city.name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectCities;
