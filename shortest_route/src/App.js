import React, { useState, useEffect, useRef } from "react";
import { MapContainer, TileLayer, Polyline, LayerGroup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";
import { citiesRawInit, pathsRawInit } from "./texts";
import "leaflet-polylinedecorator";
import useShortestPath from "./useShortestPath";
import SelectCities from "./SelectCities";
import "leaflet-easyprint";

const App = () => {
  const [startPoint, setStartPoint] = useState("");
  const [endPoint, setEndPoint] = useState("");
  const [center] = useState({ lat: 45.568624, lng: 19.6523 });
  const [isMapInitialized, setIsMapInitialized] = useState(false);
  const ZOOM_LEVEL = 9;
  const mapRef = useRef();
  const shortestPathLayerRef = useRef();

  const [cities, setCities] = useState([]);
  const [paths, setPaths] = useState([]);

  useEffect(() => {
    const rows = citiesRawInit.split("\n").map((row) => {
      const [name, lat, lng] = row
        .substring(row.indexOf("=") + 1, row.length)
        .split(",");

      return {
        name: name.trim(),
        lat: parseFloat(lat.trim()),
        lng: parseFloat(lng.trim()),
      };
    });

    setCities(rows);
  }, []);

  useEffect(() => {
    const rows = pathsRawInit.split("\n").map((row) => {
      const [from, to, distance, time] = row
        .substring(row.indexOf("=") + 1, row.length)
        .split(",");

      return {
        from: from.trim(),
        to: to.trim(),
        distance: distance.trim(),
        time: time.trim(),
      };
    });

    setPaths(rows);
  }, []);

  const citiesDictionary = Object.fromEntries(
    cities.map(({ name, lat, lng }) => [name, { lat, lng }])
  );

  const calculateShortestPath = useShortestPath();

  useEffect(() => {
    if (!isMapInitialized) return;

    shortestPathLayerRef.current.clearLayers();

    const shortestPath = calculateShortestPath(startPoint, endPoint, paths);

    const pathPoints = shortestPath
      .map((city, index) => {
        const currentCity = citiesDictionary[city];
        const nextCity = citiesDictionary[shortestPath[index + 1]];

        if (!nextCity) return undefined;

        return [
          [currentCity.lat, currentCity.lng],
          [nextCity.lat, nextCity.lng],
        ];
      })
      .filter((p) => p);

    const arrow = L.polylineDecorator(pathPoints, {
      patterns: [
        {
          offset: "50%",
          repeat: 0,
          symbol: L.Symbol.arrowHead({
            pixelSize: 15,
            polygon: false,
            pathOptions: {
              stroke: true,
              weight: 3,
              color: "#000",
            },
          }),
        },
      ],
    });

    const shortestPathLine = L.polyline(pathPoints, {
      color: "#FF0303",
      weight: 3,
      opacity: 0.5,
      smoothFactor: 1,
    });

    arrow.addTo(shortestPathLayerRef.current);
    shortestPathLine.addTo(shortestPathLayerRef.current);
  }, [
    isMapInitialized,
    citiesDictionary,
    calculateShortestPath,
    startPoint,
    endPoint,
    paths,
  ]);

  const printRef = useRef();

  const downloadMapImage = () => {
    if (!isMapInitialized) return;

    printRef.current.printMap("CurrentSize", "map");
  };

  useEffect(() => {
    if (!isMapInitialized) return;

    var printPlugin = L.easyPrint({
      hidden: false,
      exportOnly: true,
      sizeModes: ["Current", "A4Portrait", "A4Landscape"],
    }).addTo(mapRef.current);

    printRef.current = printPlugin;
  }, [isMapInitialized]);

  return (
    <div>
      <div>
        <SelectCities
          startPoint={startPoint}
          setStartPoint={setStartPoint}
          endPoint={endPoint}
          setEndPoint={setEndPoint}
          cities={cities}
        />
      </div>
      <button onClick={downloadMapImage}>Download Map Image</button>
      <div>
        <MapContainer
          center={center}
          zoom={ZOOM_LEVEL}
          ref={mapRef}
          whenReady={() => setIsMapInitialized(true)}
        >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="© OpenStreetMap contributors"
          />
          {paths.map((path) => {
            const from = citiesDictionary[path.from];
            const to = citiesDictionary[path.to];
            return (
              <Polyline
                key={`${from.lat} + ${from.lng} + ${to.lat} + ${to.lng}`}
                color="#5770c8"
                positions={[
                  [from.lat, from.lng],
                  [to.lat, to.lng],
                ]}
              />
            );
          })}
          <LayerGroup ref={shortestPathLayerRef} />
        </MapContainer>
      </div>
    </div>
  );
};

export default App;
