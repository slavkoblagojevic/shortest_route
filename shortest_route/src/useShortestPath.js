import { useCallback } from "react";

const useShortestPath = () => {
  // const [graph, setGraph] = useState({ nodes: [], edges: [] });
  // const [path, setPath] = useState([]);

  const findIt = useCallback((start, end, connections) => {
    if (!start.length || !end.length) return [];
    // Create the graph data structure
    const nodes = [
      ...new Set(connections.flatMap((conn) => [conn.from, conn.to])),
    ];
    const edges = connections.map((conn) => ({
      from: conn.from,
      to: conn.to,
      weight: parseFloat(conn.distance),
      time: parseFloat(conn.time),
    }));

    const graph = { nodes, edges };

    // Find the shortest path using Dijkstra's algorithm
    const unvisited = new Set(graph.nodes);
    const distances = graph.nodes.reduce((map, node) => {
      map[node] = node === start ? 0 : Infinity;
      return map;
    }, {});

    while (unvisited.size > 0) {
      const curr = Array.from(unvisited).reduce((a, b) =>
        distances[a] < distances[b] ? a : b
      );

      unvisited.delete(curr);
      graph.edges
        .filter((edge) => edge.from === curr)
        .forEach((edge) => {
          const dist = distances[curr] + edge.weight;
          if (dist < distances[edge.to]) {
            distances[edge.to] = dist;
          }
        });
    }

    const shortestPath = [];
    let curr = end;
    while (curr !== start) {
      shortestPath.unshift(curr);
      curr = graph.edges.find(
        (edge) =>
          edge.to === curr &&
          distances[edge.from] + edge.weight === distances[curr]
      ).from;
    }
    shortestPath.unshift(start);
    return shortestPath;
  }, []);

  return findIt;
};

export default useShortestPath;
